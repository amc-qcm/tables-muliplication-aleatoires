#!/usr/bin/lua5.3
local randTables = {}

-- keep access on used global functions when changing global env
randTables.math = math
randTables.table = table
randTables.load = load
randTables.pairs = pairs

-- change global environnement. Used to keep globale variables from the module.
_ENV = randTables

-- list of numbers used to generate operations.
-- Repetitions allow little control of probability.
nombres = { 2, 3, 4, 5, 6, 6, 7, 7, 8, 8, 9, 9 }

-- list of operations. TODO: use the 4.
-- operateurs = { '+', '-', '*', '/', '%'  }
operateurs = { '*' }

-- keep the list of operations.
operations = {}

-- used to process one operation.
current = nil

-- change operation to process
function next ()
   current = table.remove(operations)
end

-- print a tex string for the operation.
function texprint(op)
   op = op or current
   return op[1] .. '\\times ' .. op[3]
end

-- used to give wrong results, close of the real one.
-- return  a or -a, randomly
function shift(a)
	 return math.random(0,1)*2*a - a
end

-- give result shifted dec.
function value(dec, op)
   op = op or current
   return evalMath(op[1] .. op[2] .. op[3] ..'+' .. shift(dec) )
end

-- return the value of the string of mathematical expression.
function evalMath(exp)
  return load('return ' .. exp)()
end


function empty (tab)
   for k in pairs (tab) do
      tab[k] = nil
   end
end

-- initialisation.
-- make sure n is less than the square of the number of differents numbers
-- in the nombres table. TODO: add a check on n.
function initRandomTables(n)
  local i = 0

  -- seen is used to keep track of already selected operation.
  -- insure a particular operation is in the list only once.
  local seen = {}

  -- reinit current
  current = nil
  -- suppress all elements of table operations
  empty(operations)
  
  i = 0
  while i <n do
    local n1 = nombres[math.random(#nombres)]
    local n2 = nombres[math.random(#nombres)]
    local op = operateurs[math.random(#operateurs)]

    key = n1 .. op .. n2
    if seen[key] == nil then
       seen[key] = true
       table.insert(operations,{n1, op, n2})
       i = i + 1
    end    
  end
end




-- pseudo-random initialisation; Same seed, same values.
math.randomseed(1234)

return randTables
