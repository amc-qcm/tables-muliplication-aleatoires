#!/usr/bin/lua5.3
local randMultiples = {}

-- keep access on used global functions when changing global env
randMultiples.math = math
randMultiples.table = table
randMultiples.load = load
randMultiples.pairs = pairs

-- change global environnement. Used to keep globale variables from the module.
_ENV = randMultiples

-- list of numbers used to generate base of multiples.
-- Repetitions allow little control of probability.
of = { 2, 3, 4, 5, 6, 6, 7, 7, 8, 8, 9, 9 }

-- list of numbers used to generate base of multiples.
-- Repetitions allow little control of probability.
by = { 1, 2, 3, 4, 5, 6, 6, 7, 7, 8, 8, 9, 9 }

-- keep the list of operandes.
-- last one will be the number  be multiply.
operandes = {}

-- used to process one operation.
current = nil

-- change operation to process
function next ()
   current = table.remove(operandes)
end

-- change multiple
function nextMultiple(op)
   op = op or current
   table.remove(op,1)
end

-- used to give wrong results, close of the real one.
-- return  a or -a, randomly
function shift(a)
	 return math.random(0,1)*2*a - a
end

-- give a good or  bad result, following dec value.
function value(dec, op)
   op = op or current
   return evalMath(op[1] .. '*' .. op[#op] ..'+' .. shift(dec) )
end

-- return the value of the string of mathematical expression.
function evalMath(exp)
  return load('return ' .. exp)()
end

function empty (tab)
   for k in pairs (tab) do
      tab[k] = nil
   end
end

function multipleOf(oplist)
  oplist =  oplist or current
  return oplist[#oplist]
end

-- initialisation.
-- make sure nq is less than the number of differents elements of of table.
-- make sure nc is less than the number of differents elements of by table.
-- TODO: add a check on nq and nc.
function initMultiples(nq, nc)
  local i = 0
  local j = 0
  
  -- seen is used to keep track of already selected operation.
  -- insure a particular operation is in the list only once.
  local seenof = {}
  local seenby = {}

  -- reinit current
  current = nil
  -- suppress all elements of table operations
  empty(operandes)
  empty(seenof)
  
  i = 0
  while i <nq do
    local oplist = {}
    local n1 = of[math.random(#of)]
    
    if seenof[n1] == nil then
       seenof[n1] = true

       table.insert(oplist, n1)

       empty(seenby)

       j = 0
       while j<nc do
          local n2 = by[math.random(#by)]

          if seenby[n2] == nil then
             seenby[n2] = true
             table.insert(oplist,1, n2)
	     j = j+1
	  end 
       end
       table.insert(operandes, oplist)
       i = i + 1
    end
  end
end

-- pseudo-random initialisation; Same seed, same values.
math.randomseed(1234)

return randMultiples
